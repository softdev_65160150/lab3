/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab3;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab3 {
    static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static char currentPlayer = 'X';
    static int row,col;
    private static char currenPlayer;
    
    static void printWelcome(){
        System.out.println("Welcome OX");
    }
    
    static void printTable(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println("");
        }
    }
    
    static void printTurn(){
        System.out.println(currentPlayer+" Turn");
    }
    
    static void inputRowCol(){
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("Please input row col: ");
            row = sc.nextInt();
            col = sc.nextInt();
            if(table[row-1][col-1]=='-'){
               table[row-1][col-1] = currentPlayer;
                break;
            }
        }
    }
    
     static boolean checkWin(char[][] table, char currentPlayer) {
        if(checkRow(table,currentPlayer)||checkCol(table,currentPlayer)||checkDiag(table,currentPlayer)){
            return true;
        }
        return false;
    }
     
     static boolean checkRow(char[][] table,char currentPlayer){
        for(int row=0; row<3; row++){
            if(checkRow(table,currentPlayer,row)){
                return true;
            }
        }
        return false;
    }


    static boolean checkRow(char[][] table, char currentPlayer,int row) {
        for(int col=0; col<3; col++){
            if(table[row][col]!=currentPlayer){
                return false;
            } 
        }
        return true;
    }
    
    static boolean checkCol(char[][] table,char currentPlayer){
        for(int col=0; col<3; col++){
            if(checkCol(table,currentPlayer,col)){
                return true;
            }
        }
        return false;
    }
    
    
     static boolean checkCol(char[][] table,char currentPlayer,int col){
        for(int row=0; row<3; row++){
            if(table[row][col]!=currentPlayer){
                return false;
            }
        }
        return true;
    }

     static boolean checkDiag(char[][] table,char currentPlayer){
        if(table[0][0]==currentPlayer&&table[1][1]==currentPlayer&&table[2][2]==currentPlayer){
            return true;
        }
        if(table[0][2]==currentPlayer&&table[1][1]==currentPlayer&&table[2][0]==currentPlayer){
            return true;
        }
        return false;
    }
    
    static boolean checkDraw(char[][] table,char currentPlayer){
        if(checkDraw(table)){
            if(checkDraw(table,currentPlayer)){
                return false;
            }
            return true;
        }
        return false;
    }
    
    static boolean checkDraw(char[][] table){
        int round=0;
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                if(table[i][j]!='-'){
                    round++;
                }
            }
        }
        
        if(round==9){
            return true;
        }
        return false;
    }

    
    
    static void switchPlayer(){
        if(currentPlayer=='X'){
            currentPlayer='O';
        }else{
            currentPlayer = 'X';
        }
    }
    
    static void printWin(){
        System.out.println(currentPlayer+" Win!!");        
    }
    
    static void printDraw(){
        System.out.println("Draw!!");
    }
    
    static boolean inputContinue(){
        Scanner sc = new Scanner(System.in);
        System.out.print("continue(y/n) : ");
        char isContinue = sc.next().charAt(0);
        if(isContinue=='y'){
            return true;
        }
        return false;
    }
    
    static void setTable(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
               table[i][j]= '-';
            }
        }
    }

    

    public static void main(String[] args) {
       printWelcome();
        while (true) {
            while (true) {
                printTable();
                printTurn();
                inputRowCol();
                if (checkWin(table,currentPlayer)) {
                    printTable();
                    printWin();
                    break;
                }
                if(checkDraw(table,currentPlayer)){
                    printTable();
                    printDraw();
                    break;
                } 

                switchPlayer();

            }
            if (inputContinue()) {
                setTable();
            } else {
                break;
            }
        }
    }
    }

